#include <stdlib.h>
#include <stdio.h>
#include "disk_driver.h"

void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks){
	
}

int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num){
	
}

int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num){
	
}

int DiskDriver_freeBlock(DiskDriver* disk, int block_num){
	
}

int DiskDriver_getFreeBlock(DiskDriver* disk, int start){
	
}

int DiskDriver_flush(DiskDriver* disk){
	
}
