#include <stdlib.h>
#include <stdio.h>
#include "simplefs.h"

DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk){
	
}

void SimpleFS_format(SimpleFS* fs){
	
}

FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename){
	
}

int SimpleFS_readDir(char** names, DirectoryHandle* d){
	
}

FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename){
	
}

int SimpleFS_close(FileHandle* f){
	
}

int SimpleFS_write(FileHandle* f, void* data, int size){
	
}
int SimpleFS_read(FileHandle* f, void* data, int size){
	
}

int SimpleFS_seek(FileHandle* f, int pos){
	
}

int SimpleFS_changeDir(DirectoryHandle* d, char* dirname){
	
}

int SimpleFS_mkDir(DirectoryHandle* d, char* dirname){
	
}

int SimpleFS_remove(SimpleFS* fs, char* filename){
	
}
